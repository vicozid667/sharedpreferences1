package com.example.sharedpreferencesa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText etEmail;
    private Button btnGuardar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inicializarVistas();
    }

    private void inicializarVistas() {
        etEmail = findViewById(R.id.etEmail);
        btnGuardar = findViewById(R.id.btnGuardar);

        usoSharedPreference();


    }

    private void usoSharedPreference() {
        // siempre que el usuario presione el boton guardar, deberia
        // almacenar la informacion de ese campo EditText en nuestro
        // archivo de tipo SharedPreferences.
        // Siempre que la aplicacion se inicie, deberia mostrarnos esa
        // informacion persistente.
        // Nosotros vamos a controlar la gestion de ese archivo
        // si no existe lo va a crear, y si existe lo va a leer........

        //getSharedPreferences nos va a permitir obtener la referencia
        //que se haya guardado o caso contrario la va a crear.......
        SharedPreferences preferencias = getSharedPreferences(
                "datos", // primer valor un identificador al archivo sharedpreference
                Context.MODE_PRIVATE// segundo valor es el tipo de acceso al archivo en cuestion
        );
        // esto nos va a permitir recuperar los datos que hayamos almacenado
        // en ese archivo de tipo sharedPreferences

        //para obtener un valor en ese archivo, bajo el concepto
        // de clave-valor..........
        etEmail.setText(preferencias.getString("mail", ""));
        // metodo de la clase sharedPreferences getString
        // espera dos parametros:
        // el nombre de la clave o identificador de ese valor que quieren recuperar
        // un valor por defecto por si no existiera nada guarado con esa clave...

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guardarDato();
            }
        });
    }

    private void guardarDato() {
        SharedPreferences preferencias = getSharedPreferences(
                "datos",
                Context.MODE_PRIVATE
        );

        //vamos a editar el archivo para pasarle un nuevo valor.........
        // esto se realiza gracias a una clase de tipo (Editor)
        // sin este tipo de accion no podrian modificar su archivo...
        SharedPreferences.Editor editor = preferencias.edit();

        //putString sirve para actualizar el valor referido a un identificador
        // en el archivo SharedPreference.......
        editor.putString("mail", etEmail.getText().toString());

        //vamos a confirmar que lo que acabamos de recibir del campo
        //etEmail lo queremos guardar y escribir en la referencia mail
        // del archivo datos.............
        editor.commit();

        //finaliza una actividad........
        finish();

    }
}
